import re
import sys
import requests 
import telegram 
import os 
import subprocess
from dotenv import load_dotenv
from datetime import datetime
import discord
 
load_dotenv()


TELEGRAM_TOKEN=os.getenv('TELEGRAM_TOKEN')

'''
1. Add to bot to chat
2. curl https://api.telegram.org/botTELEGRAM_TOKEN/getUpdates |jq and look for chatid
'''
CHAT_ID=os.getenv('CHAT_ID')

WALLET_PASSWORD=os.getenv('WALLET_PASSWORD')

HASHRATE_PATH=os.getenv('HASHRATE_PATH')
MINING_COST=os.getenv('MINING_COST')
TOKEN_PRICE=os.getenv('TOKEN_PRICE')
WALLET_ADDRESS=os.getenv('WALLET_ADDRESS')
BALANCE_HISTORY_FILE=os.getenv('BALANCE_HISTORY_FILE')

DISCORD_TOKEN=os.getenv('DISCORD_TOKEN')
DATE_NOW = datetime.now()

ALPH_API_KEY=os.getenv('ALPH_API_KEY')

#add Mr. GP - 22.12.21
WATT_PRICE=0.16
RIG_CONSO=175
FIAT_UNIT="CHF"

if ALPH_API_KEY is not None:
    HEADERS={'X-API-KEY':ALPH_API_KEY, 'accept': '*/*'}
else:
    HEADERS = {'accept': '*/*'}

class DiscordBot(discord.Client):

    async def on_ready(self):
        print(f"Logged in as {self.user} (ID: {self.user.id})")
        print("------") 

class TelegramBot: 
 
    def __init__(self, token, chatId): 
        self.token = token 
        self.chatId = chatId 
        self.bot = telegram.Bot(token=self.token) 
 
    def getChatId(self): 
        return self.chatId 
 
    def sendMessage(self, text): 
        self.bot.send_message(chat_id=self.getChatId(), text=text) 

# todo improve it 
def computeAvgHashrate(hashrates):
    # remove the last \n
    gpuSMI=subprocess.check_output('nvidia-smi --query-gpu=gpu_name,index,power.draw --format=csv,noheader',shell=True).decode().split('\n')[:-1]
    totalHashrate = 0
    totalGpuConso = 0
    consoPerGpu = 0
    gpus=[]
    gpusHashrate=[]
    countGPU=[0]*len(gpuSMI)
    avgPerGPU=[0]*len(gpuSMI)
    totalEfficence = 0
    gpuCount = 0

    for rate in hashrates:
       totalRate=rate.split("MH/s")[0].split(':')[1].strip()
       if re.match('^\d+$',totalRate) and totalRate != 'hashrate':
          gpus += rate.split("MH/s")[1:-1]
    
    for gpu in gpus:
      if gpu != ' ':
         hash=gpu.split('\n')[0]
         rate=hash.split(':')
         gpuName=rate[0].strip().split('gpu')

         if not(re.match('.*hashrate.*',gpuName[0])) and not(re.match('.*hashrate.*',gpuName[1])) :
            gpuIndex=int(rate[0].strip().split('gpu')[1])
            gpuRate=rate[1].strip()
            if  re.match('^\d+$',gpuRate) and int(gpuRate)>0:
               countGPU[gpuIndex]+=1
               avgPerGPU[gpuIndex]+=int(gpuRate)

    #print(gpuSMI[0].split(',')[2].strip().split(' ')[0])
    for i,gpu in enumerate(avgPerGPU):
       if gpu != ' ':
         totalHashrate+=int(avgPerGPU[i]/countGPU[i])
         consoPerGpu=float(gpuSMI[i].split(',')[2].strip().split(' ')[0])
         totalGpuConso+=consoPerGpu
         # E=kH/W
         efficient=float(avgPerGPU[i]/countGPU[i])/ consoPerGpu
         totalEfficence+=efficient
         gpusHashrate.append(f"{gpuSMI[i].split(',')[0]}: {int(avgPerGPU[i]/countGPU[i])} MH/s [E={round(efficient,2)} MH/w], {round(consoPerGpu,1)} W")
         gpuCount+=1

    # power data, by Mr GP
    powerData=   f"\n       Rig GPU conso: {round(totalGpuConso,1)} W"
    powerData+=  f"\n       Rig hardware conso: {RIG_CONSO} W"
    powerData+=  f"\n       Rig full conso: {round(totalGpuConso+RIG_CONSO,1)} W"
    powerData += f"\n       Average Efficiency: {round(totalEfficence/gpuCount,2)} MH/W"
    powerData += f"\n       Average MH price: {round((totalEfficence/gpuCount)*WATT_PRICE,2)} {FIAT_UNIT}"

    return totalHashrate,gpusHashrate,powerData
 
# in order to work the gpu-miner run-miner.sh should write the output in /var/log/gpu-miner/hashrate
def hashrate(): 
    numBlock = 0 
    hashrate = 0 
    avgHashrateNum = 0
    totalHashrate = 0 
    gpusHashrate = []
    totalGpuConso = 0
    standaloneMiner = False

    minerLog = HASHRATE_PATH
    fileHash=os.path.join(HASHRATE_PATH,'alephium.log')
    fileMined=os.path.join(HASHRATE_PATH,'mined')

    if os.path.isdir(HASHRATE_PATH):
       if os.path.exists(fileHash):
          try :
             with open(fileHash,"r") as f:
                 fileContent = f.read()

                 avgHashrate = re.findall('.*hashrate.*',fileContent)
                 if len(avgHashrate) > 0:
                    totalHashrate,gpusHashrate,powerData = computeAvgHashrate(avgHashrate)
          except :
             print(f"error with file {fileHash}")
             totalHashrate = -1
        
          if standaloneMiner:
            with open(fileHash,'r') as f:
                
                blockMined = re.findall('.*new block:.*',f.read())
                numBlock += len(blockMined)
                #store the hash of mined block
                if numBlock > 0 :
                    with open(fileMined,'a') as fm:
                        for block in blockMined:
                            fm.write(f"{block.split(':')[1].strip()};{totalHashrate};{int(datetime.timestamp(DATE_NOW))}\n")

          # todo find a better way
          if not(debug):
             os.system(f"echo ' ' > {fileHash}")
    else:
        sys.exit(f"Error: {minerLog} should be a folder")

    return totalHashrate, numBlock,gpusHashrate,powerData

def temperatureGPU():
    gpuList=list()
    temp=subprocess.check_output('nvidia-smi --query-gpu=gpu_name,temperature.gpu --format=csv,noheader',shell=True).decode().strip().split('\n')


    for gpu in temp:
       gpuList.append(f"{gpu.replace(',', ':')} °C")
    return gpuList

def getTotalAlph(hashrate):

    if WALLET_ADDRESS is not None:
        normalWalletAlph=float(requests.get(f'http://127.0.0.1:12973/addresses/{WALLET_ADDRESS}/balance',headers=HEADERS).json()["balanceHint"].replace('ALPH','').strip())

    else:
        normalWalletAlph=0

    rUnlock=requests.post('http://127.0.0.1:12973/wallets/mainnet/unlock', json={"password": f"{WALLET_PASSWORD}"},headers=HEADERS)
    minerWallet=requests.get('http://127.0.0.1:12973/wallets/mainnet/balances',headers=HEADERS)

    if minerWallet.json().get('detail'):
        requests.post('http://127.0.0.1:12973/wallets/mainnet/unlock', json={"password": f"{WALLET_PASSWORD}"},headers=HEADERS)
        minerWallet=requests.get('http://127.0.0.1:12973/wallets/mainnet/balances',headers=HEADERS)

    minerWalletAlph=float((minerWallet.json()['totalBalanceHint']).replace('ALPH','').strip())
    total = round(minerWalletAlph+normalWalletAlph,1)

    if BALANCE_HISTORY_FILE is not None:

        if os.path.exists(BALANCE_HISTORY_FILE):
                # fastest way to find last line
                lastAmount=float((subprocess.check_output(['tail', '-1', BALANCE_HISTORY_FILE])).decode().split(';')[0])
                alphWon=round(total-lastAmount,1)
                if not(debug): 
                    with open(BALANCE_HISTORY_FILE,'a+') as f:
                        f.write(f"{total};{hashrate};{int(datetime.timestamp(DATE_NOW))}\n")
        else:
                with open(BALANCE_HISTORY_FILE,'w') as f:
                    alphWon = 0
                    f.write(f"{total}\n")
    else:
         alphWon=0

    return total,alphWon


debug=False
if len(sys.argv) > 1:
   debug=sys.argv[1]

hashrate, numBlock,gpus,powerData = hashrate()
totalAlph,alphWon = getTotalAlph(hashrate)
temperature=temperatureGPU()

text=f"💰 Total: {totalAlph} ALPH\n"
text+=f"      🏆 Won: {alphWon} ALPH\n"
if TOKEN_PRICE is not None:
   if MINING_COST is None:
        MINING_COST=0
   text += f"       (estimated: {round(totalAlph*float(TOKEN_PRICE) - float(MINING_COST),1)} $)\n"

text += "\n🌡 Temperature:\n"
for gpu in temperature:
   text += f"       {gpu}\n"

if hashrate > 0:
   text += f"\n#️⃣  Hashrate: {hashrate} MH/s\n"
   for gpu in gpus:
      text += f"       {gpu}\n"
   text += "\n📊 Power data"+powerData
elif hashrate  == -1 :
   text += f"⚠️  Error with hashrate file\n"  

if numBlock > 0:
   text+=f" Number of mined blocks: {numBlock}"


if TELEGRAM_TOKEN is not None and CHAT_ID is not None:
    bot = TelegramBot(TELEGRAM_TOKEN, CHAT_ID)
    print(text)

    if not(debug):
        bot.sendMessage(text)
"""
if DISCORD_TOKEN is not None and DISCORD_CHAT_ID is not None:
    client = DiscordBot()
    client.run(DISCORD_TOKEN)
"""
