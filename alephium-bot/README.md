# ALEPHIUM Bot

Stay updated in Telegram about your [Alephium](https://alephium.org/) mining progress


<img src="./resources/telegram.png" width="350">


## Installation

In order to have this bot working, you have to: 

- Create a Telegram bot ([How to create a bot](https://medium.com/shibinco/create-a-telegram-bot-using-botfather-and-get-the-api-token-900ba00e0f39]))
- Have the gpu-miner application to redirect the output in a file (check [run-miner.sh](./run-miner.sh)) 
- python3 and pip must be installed

1. `git clone https://gitlab.com/sven-hash/alephium.git`
2. `cp .env_example .env`
3. Add informations to .env file (for HASHRATE_PATH only set the folder)
4. `pip3 install -r requirements.txt`
5. Create a cronjob to run the bot ([example](./cronjob))

To test the bot you can execute `python3 bot.py` and you should receive a message in Telegram

### Retrieve Telegram chat id

1. Add bot to the chat
2. In a browser go to https://api.telegram.org/botTELEGRAM_TOKEN/getUpdates (replace TELEGRAM_TOKEN by the one given from BotFather)
3. Find the chatid in the webpage

### Hashrate by GPU is incosistent

CUDA and nvidia-smi doesn't use the same method to index the GPU

Be sure to export the environnement variable `CUDA_DEVICE_ORDER=PCI_BUS_ID` when starting the gpu-miner process


### .env entries

For script to work, you have to set entries in a .env. It will be loaded at the launch

| Variable | Default | Description |
|----------|---------|-------------|
| `TELEGRAM_TOKEN`| | Telegram bot API Token |
| `CHAT_ID` | | id of channel or chat where the bot is |
| `WALLET_PASSWORD` | | password of the miner wallet|
| `HASHRATE_PATH` | None | folder where the gpu-miner run-miner.sh script write the output |
| `MINING_COST` | 0 | estimated cost to mine (electricity, GPU price) (thanks to @psua) |
| `PRICE_TOKEN` | 0 | estimated price of 1 ALPH, in the future real listing price will be used |
| `WALLET_ADDRESS`| None | in case of transfering ALPH to another wallet |

### Estimated Fiat value

The estimated fiat value can be calculated by adding a `PRICE_TOKEN=xx` variable in the .env file 

### Stats

The mined file written in the `HASHRATE_PATH` store the hash of all the mined block and a timestamp

Actually the script only store this informations, in the future it could be used to make stats

## Contact

Feel free to contact me at https://t.me/svvne or open issues and PR

